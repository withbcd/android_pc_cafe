package ac.kr.project_20191121.FoodOrder;

public interface OnItemClick {
    void onClick(String name, int cost);
}
